﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Orbelite.Artempresa.Datos.Modelos.Entidades.Mapeo.Abstracciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Orbelite.Artempresa.Datos.Modelos.Entidades.Mapeo
{
    public class PersonaMapeo: IEntityTypeConfigurationPersonalizado<Persona>
    {
        public void Map(EntityTypeBuilder<Persona> builder)
        {
            builder.ToTable("persona");
            builder.HasKey(e => e.IdPersona);
            builder.Property(x => x.IdPersona).HasColumnName("id_persona");
            builder.Property(x => x.IdPersona).IsRequired();

            builder.Property(x => x.DNI).HasColumnName("dni");
            builder.Property(x => x.Nombres).HasColumnName("nombres").IsRequired();
            builder.Property(x => x.ApePaterno).HasColumnName("apellido_paterno").IsRequired();
            builder.Property(x => x.ApeMaterno).HasColumnName("apellido_materno").IsRequired();

            builder.Property(x => x.Correo).HasColumnName("correo");
            builder.Property(x => x.Telefono).HasColumnName("telefono");
            builder.Property(x => x.Direccion).HasColumnName("direccion");
            builder.Property(x => x.Departamento).HasColumnName("departamento");
            builder.Property(x => x.Provincia).HasColumnName("provincia");
            builder.Property(x => x.Distrito).HasColumnName("distrito");
            builder.Property(x => x.Creado).HasColumnName("creado");
            builder.Property(x => x.Actualizado).HasColumnName("actualizado");
            builder.Property(x => x.Borrado).HasColumnName("borrado");
            builder.Property(x => x.EstaBorrado).HasColumnName("esta_borrado");
           
        }
    }
}
