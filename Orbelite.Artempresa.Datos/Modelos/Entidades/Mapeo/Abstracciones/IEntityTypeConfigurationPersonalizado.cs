﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Datos.Modelos.Entidades.Mapeo.Abstracciones
{
    public interface IEntityTypeConfigurationPersonalizado<TEntityType> where TEntityType : class
    {
        void Map(EntityTypeBuilder<TEntityType> builder);
    }
    public static class EntityMappingExtensions
    {
        public static ModelBuilder RegisterEntityMapping<TEntity, TMapping>(this ModelBuilder builder)
           where TMapping : IEntityTypeConfigurationPersonalizado<TEntity>
           where TEntity : class
        {
            var mapper = (IEntityTypeConfigurationPersonalizado<TEntity>)Activator.CreateInstance(typeof(TMapping));
            mapper.Map(builder.Entity<TEntity>());
            return builder;
        }
    }
}
