﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Datos.Modelos.Entidades
{
    public class Persona
    {
        public Int64 IdPersona { get; set; }

        public string DNI { get; set; }
        
        public string Nombres { get; set; }

        public string ApePaterno { get; set; }

        public string ApeMaterno { get; set; }

        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }

        public DateTime? Creado { get; set; }

        public DateTime? Actualizado { get; set; }

        public DateTime? Borrado { get; set; }

        public Int16 EstaBorrado { get; set; }

        public Persona()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
        }
    }
}
