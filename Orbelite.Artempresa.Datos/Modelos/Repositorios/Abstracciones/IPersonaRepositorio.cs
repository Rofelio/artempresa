﻿using Orbelite.Artempresa.Datos.Modelos.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Artempresa.Datos.Modelos.Repositorios.Abstracciones
{
    public interface IPersonaRepositorio
    {
        Task RegistrarPersona(Persona entidad);
        Task<Persona> BuscarCorreoPersona(string correo);
    }
}
