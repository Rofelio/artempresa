﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Artempresa.Datos.Infraestructura.Conexiones;
using Orbelite.Artempresa.Datos.Modelos.Entidades;
using Orbelite.Artempresa.Datos.Modelos.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Artempresa.Datos.Modelos.Repositorios.Implementaciones
{
    public class PersonaRepositorio: IPersonaRepositorio
    {
        private readonly ConexionArtempresa _conexionArtempresa;

        public PersonaRepositorio(ConexionArtempresa conexionArtempresa)
        {
            _conexionArtempresa = conexionArtempresa;
        }

        public async Task RegistrarPersona(Persona entidad)
        {
            try
            {
                _conexionArtempresa.Database.EnsureCreated();
                _conexionArtempresa.Personas.Add(entidad);
                _conexionArtempresa.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Persona> BuscarCorreoPersona(string correo)
        {
            try
            {
                var  entidad = await _conexionArtempresa.Personas.Where(e => e.Correo == correo).FirstOrDefaultAsync();
                return entidad;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
