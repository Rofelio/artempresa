﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Artempresa.Datos.Modelos.Entidades;
using Orbelite.Artempresa.Datos.Modelos.Entidades.Mapeo;
using Orbelite.Artempresa.Datos.Modelos.Entidades.Mapeo.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Datos.Infraestructura.Conexiones
{
    public class ConexionArtempresa: DbContext
    {
        public ConexionArtempresa(DbContextOptions<ConexionArtempresa> options) : base(options)
        {
        }
        public DbSet<Persona> Personas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.RegisterEntityMapping<Persona, PersonaMapeo>();
            //base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Pedidos>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //});
        }

    }
}
