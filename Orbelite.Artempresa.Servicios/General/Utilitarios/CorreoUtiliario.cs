﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Orbelite.Artempresa.Servicios.General.Utilitarios
{
    public class CorreoUtiliario
    {
        public static bool EnviarCorreo(string asunto, List<string> destinatarios, string cuerpo)
        {
            var val = true;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("rofpm2016@gmail.com", "Coolmena2019"),
                Timeout = 20000
            };

            using (var message = new MailMessage()
            {
                From = new MailAddress("rofpm2016@gmail.com"),
                Subject = asunto,
                Body = cuerpo
            })
            {
                message.IsBodyHtml = true;
                destinatarios.ForEach(e => message.To.Add(e));

                try
                {
                    smtp.Send(message);
                }
                catch (Exception ex)
                {
                    val = false;

                    throw new Exception("No se ha podido enviar el  email: " + ex.Message);
                }
                finally
                {
                    smtp.Dispose();
                }
            }
            return val;
        }
    }
}
