﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresas.Servicios.General.Dtos
{
    public class RespuestaSimpleDto
    {
        [JsonProperty(PropertyName = "mensaje")]
        public string Mensaje { get; set; }

        [JsonProperty(PropertyName = "id")]
        public bool  Suceso { get; set; }
    }
}
