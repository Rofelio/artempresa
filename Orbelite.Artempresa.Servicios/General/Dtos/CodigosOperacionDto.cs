﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Servicios.General.Dtos
{
    public enum CodigosOperacionDto
    {
        Suceso,
        ResultadoVacio,
        UsuarioIncorrecto,
        UsuarioInhabilitado,
        OperacionNoDisponible,
        AccesoInvalido,
        NoExiste,
        ErrorServidor,
        Invalido,
        DniInexistente = 410,
        NoSePudoRealizarPago = 1034,
        CamposRequeridos = 400
    };
}
