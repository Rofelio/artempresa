﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Servicios.General.Dtos
{
    public class AppConfiguracionesDto
    {
        public string RutaTemplateHtml { get; set; }
    }
}
