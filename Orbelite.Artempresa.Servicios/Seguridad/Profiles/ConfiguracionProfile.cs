﻿using Orbelite.Artempresa.Datos.Modelos.Entidades;
using Orbelite.Artempresa.Servicios.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Servicios.Seguridad.Profiles
{
    public class ConfiguracionProfile:AutoMapper.Profile
    {
        public ConfiguracionProfile()
        {
            CreateMap<RegistroPersonaPeticionDto, Persona>().ReverseMap();
        }
    }
}
