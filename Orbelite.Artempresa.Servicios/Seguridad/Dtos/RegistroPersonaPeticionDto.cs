﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Artempresa.Servicios.Seguridad.Dtos
{
    public class RegistroPersonaPeticionDto
    {
        [JsonProperty(PropertyName = "nombres")]
        public string Nombres { get; set; }

        [JsonProperty(PropertyName = "apePaterno")]
        public string ApePaterno { get; set; }

        [JsonProperty(PropertyName = "apeMaterno")]
        public string ApeMaterno { get; set; }

        [JsonProperty(PropertyName = "correo")]
        public string Correo { get; set; }

        [JsonIgnore]
        public string UrlHtml { get; set; }
    }
}
