﻿using Newtonsoft.Json;

namespace Orbelite.Artempresa.Servicios.Seguridad.Dtos
{
    public class RegistroPersonaRespuestaDto
    {
        [JsonProperty(PropertyName = "mensaje")]
        public string Mensaje { get; set; }

        [JsonProperty(PropertyName = "suceso")]
        public bool Suceso { get; set; }
    }
}
