﻿using AutoMapper;
using Orbelite.Artempresa.Datos.Modelos.Entidades;
using Orbelite.Artempresa.Datos.Modelos.Repositorios.Abstracciones;
using Orbelite.Artempresa.Servicios.General.Dtos;
using Orbelite.Artempresa.Servicios.General.Utilitarios;
using Orbelite.Artempresa.Servicios.Seguridad.Dtos;
using Orbelite.Artempresa.Servicios.Seguridad.Servicios.Abstracciones;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Artempresa.Servicios.Seguridad.Servicios.Implementaciones
{
    public class PersonaServicio: IPersonaServicio
    {
        private readonly IPersonaRepositorio _personaRepositorio;
        private readonly IMapper _mapper;
        private readonly AppConfiguracionesDto _appConfiguraciones;

        public PersonaServicio(
                        IPersonaRepositorio personaRepositorio,
                        IMapper mapper,
                        AppConfiguracionesDto appConfiguraciones
            )
        {
            _personaRepositorio = personaRepositorio;
            _mapper = mapper;
            appConfiguraciones = _appConfiguraciones;
        }

        public async Task<OperacionDto<RegistroPersonaRespuestaDto>> RegistrarPersona(RegistroPersonaPeticionDto peticion)
        {
            var validaModelo = ValidacionUtilitario.ValidarModelo<RegistroPersonaRespuestaDto>(peticion);
            if (!validaModelo.Completado)
            {
                return new OperacionDto<RegistroPersonaRespuestaDto>(CodigosOperacionDto.NoExiste, "Debe ingresar  todos los datos.");
            }

            var pers= await _personaRepositorio.BuscarCorreoPersona(peticion.Correo);

            if (pers != null)
            {
                return new OperacionDto<RegistroPersonaRespuestaDto>(CodigosOperacionDto.NoExiste, "Ya se encuentra registrado su cuenta.");
            }


            var per = new Persona();
            per = _mapper.Map<Persona>(peticion);
            await _personaRepositorio.RegistrarPersona(per);


            var html = File.ReadAllText(peticion.UrlHtml);

            html = html.Replace("%%NOMBRE%%", $"{ peticion.Nombres} { peticion.ApePaterno} { peticion.ApeMaterno}");

            var asunto = "Artempresa";
            var cuerpo = html;

            var destinatarios = new List<string>();
            destinatarios.Add(peticion.Correo);

            CorreoUtiliario.EnviarCorreo(asunto, destinatarios, cuerpo);


            return new OperacionDto<RegistroPersonaRespuestaDto>(new RegistroPersonaRespuestaDto()
            {
                Mensaje = "El proceso se realizó con éxito...",
                Suceso = true
            });
        }

    }
}
