﻿using Orbelite.Artempresa.Servicios.General.Dtos;
using Orbelite.Artempresa.Servicios.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Artempresa.Servicios.Seguridad.Servicios.Abstracciones
{
    public interface IPersonaServicio
    {
        Task<OperacionDto<RegistroPersonaRespuestaDto>> RegistrarPersona(RegistroPersonaPeticionDto peticion);
    }
}
