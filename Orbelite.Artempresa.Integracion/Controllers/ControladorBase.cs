﻿using Microsoft.AspNetCore.Mvc;
using Orbelite.Artempresa.Servicios.General.Dtos;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Artempresa.Integracion.Controllers
{
 
    public class ControladorBase: Controller
    {
        public ActionResult<T> DevolverResultadoOMostrarErrores<T>(OperacionDto<T> operacion)
        {
            if (operacion.Completado)
            {
                return operacion.Resultado;
            }

            return BadRequest(new
            {
                mensajes = operacion.Mensajes
            });
        }

        //protected string ObtenerTokenAcceso()
        //{

        //    var token = default(string);

        //    if (Request.Headers.ContainsKey("token-acceso"))
        //    {
        //        token = Request.Headers["token-acceso"];
        //    }

        //    return token;
        //}
        //protected string ObtenerPESignature()
        //{

        //    var Pe_signature = default(string);

        //    if (Request.Headers.ContainsKey("PE-Signature"))
        //    {
        //        Pe_signature = Request.Headers["PE-Signature"];
        //    }

        //    return Pe_signature;
        //}
        protected JsonResult ObtenerResultadoValidoDeOperacionComoJsonResult<T>(OperacionDto<T> operacionDto)
        {
            return new JsonResult(new
            {
                suceso = true,
                dato = operacionDto.Resultado
            });
        }
        protected JsonResult ObtenerResultadoOGenerarErrorDeOperacionComoJsonResult<T>(OperacionDto<T> operacion)
        {
            if (!operacion.Completado)
            {

                return new JsonResult(new
                {
                    suceso = false,
                    mensajes = operacion.Mensajes
                });

            }
            return ObtenerResultadoValidoDeOperacionComoJsonResult(operacion);
        }
    }
}
