﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Artempresa.Servicios.Seguridad.Dtos;
using Orbelite.Artempresa.Servicios.Seguridad.Servicios.Abstracciones;

namespace Orbelite.Artempresa.Integracion.Controllers
{
    public class HomeController : ControladorBase
    {
        private readonly IPersonaServicio _personaServicio;
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(
                        IPersonaServicio personaServicio,
                        IHostingEnvironment hostingEnvironment
            )
        {
            _personaServicio = personaServicio;
            _hostingEnvironment = hostingEnvironment;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RegistrarDatosPersonales(RegistroPersonaPeticionDto peticion)
        {
            peticion.UrlHtml= $"{_hostingEnvironment.WebRootPath}\\plantillas_correo\\correo_suscripcion1.html";
            var operacion = await _personaServicio.RegistrarPersona(peticion);
            return ObtenerResultadoOGenerarErrorDeOperacionComoJsonResult(operacion);
        }
    }
}