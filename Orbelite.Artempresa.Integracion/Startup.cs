﻿using System;
using System.IO;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orbelite.Artempresa.Datos.Infraestructura.Conexiones;
using Orbelite.Artempresa.Integracion.Infraestructura.Autofac;
using Orbelite.Artempresa.Servicios.General.Dtos;
using Orbelite.Artempresa.Servicios.Seguridad.Profiles;

namespace Orbelite.Artempresa.Integracion
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(8);
            });

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ConfiguracionProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddDbContext<ConexionArtempresa>(options => options.UseMySQL(Configuration.GetConnectionString("artempresa")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var builder = new ContainerBuilder();
            builder.Populate(services);
            cargarDatosConfiguracion(builder);
            builder.RegisterModule(new RepositoriosModule());
            builder.RegisterModule(new ServiciosModule());

            ApplicationContainer = builder.Build();


            return new AutofacServiceProvider(ApplicationContainer);
        }

        private void cargarDatosConfiguracion(ContainerBuilder builder)
        {
            builder.Register(c => new AppConfiguracionesDto()
            {
                RutaTemplateHtml = Configuration["Template:RUTA_TEMPLATE_HTML_SUSCRIPCION"]
            }).InstancePerLifetimeScope();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/Error/{0}");
            }

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
