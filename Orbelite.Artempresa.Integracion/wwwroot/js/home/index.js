﻿function estamosTrabajando() {
    swal("!Aviso¡", "Estamos trabajando !", "warning");
}

function Limpiar() {
    $("#nombres").val("");
    $("#apellidoPaterno").val("");
    $("#apellidoMaterno").val("");
}

function ValidarCorreo() {
    var flag = true;
    if ($("#txtcorreo").val() == "") {
        $("#txtcorreo").focus();
        mensaje_alerta("!Aviso¡", "Especifica su correo!", "warning");
        flag = false;
    }else if (!verificaformato_email($("#txtcorreo").val()) ) {
        $("#txtcorreo").focus();
        mensaje_alerta("!Aviso¡", "Asegúrate de que la direccion tenga el formato correcto!", "warning");
        flag = false;
    }
    return flag;
}

function AbrirModal() {
    if (ValidarCorreo()) {
        Limpiar();
        $("#Modal_Registrarse").modal("show");
    }
}
function CerrarModal() {

    Limpiar();
    $("#Modal_Registrarse").modal("hide");

}

function LlenarDatos() {
    var data = {
        "correo": $("#txtcorreo").val(),
        "nombres": $("#nombres").val(),
        "apePaterno": $("#apellidoPaterno").val(),
        "apeMaterno": $("#apellidoMaterno").val()
    }
    return data;
}
function validarCampos() {
    var flag = true;
    if ($("#nombres").val() == "") {
        $("#nombres").focus();
        mensaje_alerta("!Aviso¡", "Especifica su Nombre!", "warning");
        flag = false;
    } else if ($("#apellidoPaterno").val() == "") {
        $("#apellidoPaterno").focus();
        mensaje_alerta("!Aviso¡", "Especifica su Apellido Paterno!", "warning");
        flag = false;
    } else if ($("#apellidoMaterno").val() == "") {
        $("#apellidoMaterno").focus();
        mensaje_alerta("!Aviso¡", "Especifica su Apellido Materno!", "warning");
        flag = false;
    }
    return flag;
}

function GuardarUsuario() {
    if (validarCampos()) {

        bloquearPantalla("Enviando Registro...");
        $.ajax({
            type: 'POST',
            url: $("#__URL_REGISTRAR_DATOS_PERSONALES").val(),
            data: LlenarDatos(),
            success: function (data) {
                desbloquearPantalla();
                if (!data.suceso) {
                    CerrarModal();
                    $("#txtcorreo").select();
                    $("#txtcorreo").focus();
                    mensaje_alerta("!Aviso¡", data.mensajes, "warning");
                    return;
                }
                if (data.dato.suceso) {
                    mensaje_alerta("!Proceso correcto¡", data.dato.mensaje, "success");
                    CerrarModal();
                }

            }
        })
    }
}