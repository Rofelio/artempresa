﻿//function bloquearPantalla(mensaje) {
//    miMensaje = '<div class="contenedor-cargando-espera" style="background:#fff;opacity:1;"  ><img style="background:#35404800;" src="' + $('#__HDN_LOADING_GIF').val() + '" /> ';
//    if (mensaje) {
//        miMensaje += '<div>' + mensaje + '</div>';
//    }

//    miMensaje += "</div>";

//    $.blockUI({
//        baseZ: 3000,
//        message: miMensaje,
//        css: {
//            top: ($(window).height() - 101) / 2 + 'px',
//            left: ($(window).width() - 128) / 2 + 'px',
//            width: '128px',
//            border: 'none'
//        }
//    });
//}
function bloquearPantalla(mensaje) {
    miMensaje = '<div class="preloader align-items-center justify-content-center" style="background:rgba(0, 0, 0, 0.00);width: 200px !important;"><img class="loadingimg" src="' + $('#__HDN_LOADING_GIF').val() + '" width="40px"/> ';
    if (mensaje) {
        miMensaje += '<div class="loading">' + mensaje + '</div>'
    }

    miMensaje += "</div>"

    $.blockUI({
        baseZ: 3000,
        message: miMensaje,
        css: {
            top: ($(window).height() - 101) / 2 + 'px',
            left: ($(window).width() - 128) / 2 + 'px',
            width: '128px',
            border: 'none'
        }
    });
}

function desbloquearPantalla() {
    $.unblockUI();
}

function realizarPost(url, datos, tipoRespuesta, fSuceso, fError, timeoutAjax, noRedireccionar) {

    var timeoutDefecto = 1000 * 60;
    if (timeoutAjax) {
        timeoutDefecto = 1000 * timeoutAjax;
    }
    //console.log(timeoutDefecto);

    var xhr = $.ajax({
        type: "POST",
        url: url,
        dataType: tipoRespuesta,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(datos),
        success: function (data) {
            if (fSuceso) {
                fSuceso(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + ': ' + errorThrown);
            desbloquearPantalla();

            if (jqXHR.status == 403) {
                alert("Su sesión acaba de expirar. Por favor ingrese de nuevo su login");
                var respuestaRedireccion = JSON.parse(jqXHR.responseText);
                location.href = respuestaRedireccion.urlLogin;
            }

            if (fError) {
                fError(jqXHR, textStatus, errorThrown);
            }

        },
        timeout: timeoutDefecto
    });
    return xhr;
}

function mostrarErroresDeResultado(idContenedor, resultado) {
    var html = "";
    for (i = 0; i < resultado.mensajes.length; i++) {
        html += resultado.mensajes[i] + '<br/>';
    }

    $('#' + idContenedor + ' .inner').html(html);
    $('#' + idContenedor).show();
}

function ponerInputSoloNumeros(selector) {
    $(selector).on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
}

function ponerParaIngresarSoloPlaca(idItem) {
    $('#' + idItem).keydown(function (e) {
        //if (e.shiftKey || e.ctrlKey || e.altKey) {
        //    e.preventDefault();
        //} else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
            e.preventDefault();
        }

        if (key === 32) {
            e.preventDefault();
        }
        //}
    });
}


function obtenerValorOVacio(valor) {
    if (!valor) {
        return "";
    }

    return valor;
}

function obtenerValorONumero(valor) {
    if (!valor) {
        return 0;
    }

    return valor;
}


function obtenerEstadoNombreSms(idEstado) {
    var estado = "";
    switch (idEstado) {
        case 1:
            estado = "Creado";
            break;
        case 2:
            estado = "Procesado";
            break;
        case 3:
            estado = "Enviado";
            break;
        case 4:
            estado = "Recibido";
            break;
        case 5:
            estado = "Invalido";
            break;
        case 6:
            estado = "Clickeado";
            break;
    }

    return estado;
}

/************************************************ Funciones Aumentadas para Coolmena ***************************/
function FechaSistema() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = mm + '/' + dd + '/' + yyyy ;
    return today;
}

function FechaSistemaPreguntas() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var fecha = new Date();
    var horas = fecha.getHours();
    var minutos = fecha.getMinutes();
    //var segundos = fecha.getSeconds();
    var today = horas + ':'+minutos+' '+ mm + '/' + dd + '/' + yyyy;
    return today;
}

function verificaformato_email(cadena) {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    var flag = true;
    if (!emailRegex.test(cadena)) {
        flag = false;
    }
    return flag;
}

function _Next(campo) {
    if (event.key === 'Enter' || event.keyCode == 13) {
        $(campo).focus();
    }
}

function OcultarCajaError() {
    $('#contenedorError').hide();
}

function BorrarDatosSegunCodigo(s) {
    if (event.which == 8 || event.which == 46) {
        document.getElementById(s).value = "";
    }
}

function goBack() {
    window.history.back();
}
//******************************************** VALIDACION DE NUMEROS **********************************
function SoloNumeros1_9() {
    var key = window.event ? event.which : event.keyCode;
    if (key < 48 || key > 57) {
        event.preventDefault();
    }
}
//******************************************** VALIDACION DE NUMEROS - PUNTO **********************************
function SoloNumeros_Punto() {
    var key = window.event ? event.which : event.keyCode;
    if (key < 48 || key > 57) {
        if (key !== 46) {
            event.preventDefault();
        }
    }
}

