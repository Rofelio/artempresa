﻿//******************************************** SOBRE LOCALIZACION DE GOOGLE MAPS **********************************

var map;
var markers = [];
function initMap() {
    var bangalore = { lat: -12.0431800, lng: -77.0282400 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: bangalore
    });
}
function geocodeAddress(dir) {
    var address = dir;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: results[0].geometry.location
            });
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                title: 'Punto localizado!'
            });
        } else {

            mensaje_alerta("Error en la dirección", "Google Maps no puede localizar la dirección ingresada.", "error");
        }
    });
}